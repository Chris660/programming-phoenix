Setup notes for Fedora
======================

### 1. Install the server:

```
sudo dnf install postgresql-server
```


### 2. Initialise the "cluster"

```
$ sudo postgresql-setup --initdb
* Initializing database in '/var/lib/pgsql/data'
* Initialized, logs are in /var/lib/pgsql/initdb_postgresql.log
```
 

### 3. Start the DB server

```
$ systemctl start postgresql
```


### 4. Create a new DB user

In this example, we create a DB user called `ecto` with permission
to create new DBs and roles.  We do this logged in as the `postgres`
Linux user:

```
$ sudo su - postgres
Create a new DB user
stgres@hp420 ~]$ createuser --createdb --createrole --pwprompt ecto
Enter password for new role: 
Enter it again: 

[postgres@hp420 ~]$ psql -c '\dg'
                                   List of roles
 Role name |                         Attributes                         | Member of 
-----------+------------------------------------------------------------+-----------
 ecto      | Create role, Create DB                                     | {}
 postgres  | Superuser, Create role, Create DB, Replication, Bypass RLS | {}
```


### 4. Allow our new user to login from `localhost` with a password

Edit `/var/lib/pgsql/data/pg_hba.conf`.  It's important to place the
new entry *ahead* of other matching `host` entries that specify the
`ident` method.

```
# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             all                                     peer

# IPv4 local connections:
host    all             ecto            127.0.0.1/32            md5
# host    all             all             127.0.0.1/32            ident

# IPv6 local connections:
host    all             ecto            ::1/128                 md5
# host    all             all             ::1/128                 ident
```

Now test the connection:

```
$ psql --host=localhost --username=ecto -l
Password for user ecto: 
                                  List of databases
   Name    |  Owner   | Encoding |   Collate   |    Ctype    |   Access privileges   
-----------+----------+----------+-------------+-------------+-----------------------
 postgres  | postgres | UTF8     | en_GB.UTF-8 | en_GB.UTF-8 | 
 template0 | postgres | UTF8     | en_GB.UTF-8 | en_GB.UTF-8 | =c/postgres          +
           |          |          |             |             | postgres=CTc/postgres
 template1 | postgres | UTF8     | en_GB.UTF-8 | en_GB.UTF-8 | =c/postgres          +
           |          |          |             |             | postgres=CTc/postgres
(3 rows)
```


### 5. Edit the environment config

```
# Configure your database
config :rumbl, Rumbl.Repo,
  username: "ecto",
  password: "***********",
  database: "rumbl_dev",
  hostname: "localhost",
  show_sensitive_data_on_connection_error: true,
  pool_size: 10
```


### 6. Create the DB using mix

Create the DB, and run our migrations.

```
$ mix ecto.create
The database for Rumbl.Repo has been created

$ mix ecto.migrate
  ...
20:27:04.501 [info]  == Migrated 20191102161143 in 0.0s
```

Let's see what that achieved:

```
$ psql --host=localhost --username=ecto rumbl_dev
Password for user ecto: 
psql (11.5)
Type "help" for help.

rumbl_dev=> \d
List of relations
 Schema |       Name        |   Type   | Owner 
--------+-------------------+----------+-------
 public | categories        | table    | ecto
 public | categories_id_seq | sequence | ecto
 public | schema_migrations | table    | ecto
 public | users             | table    | ecto
 public | users_id_seq      | sequence | ecto
 public | videos            | table    | ecto
 public | videos_id_seq     | sequence | ecto
(7 rows)
```
