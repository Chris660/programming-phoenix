defmodule Rumbl.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :name, :string
    field :username, :string

    # :virtual fields are not added to the DB, they exist only in the `struct`
    field :password, :string, virtual: true
    field :password_hash, :string

    timestamps()
  end

  def changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :username])
    |> validate_required([:name, :username])
    |> validate_length(:username, min: 1, max: 20)
    |> unique_constraint(:username)
  end

  def registration_changeset(user, params) do
    user
    |> changeset(params)
    |> cast(params, [:password])
    |> validate_required([:password])
    |> validate_length(:password, min: 6, max: 100)
    |> put_password_hash()
  end

  defp put_password_hash(cset) do
    case cset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        hashed_pass = Pbkdf2.hash_pwd_salt(pass)
        put_change(cset, :password_hash, hashed_pass)

      _ ->
        cset
    end
  end
end
