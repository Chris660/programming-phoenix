defmodule RumblWeb.Auth do
  import Plug.Conn
  import Phoenix.Controller
  alias RumblWeb.Router.Helpers, as: Routes

  def init(opts), do: opts

  def call(conn, _opts) do
    # Plug.Conn.get_session(conn, key)
    # get_session(t(), String.t() | atom()) :: any()
    #
    # Returns session value for the given key.
    # If key is not set, nil is returned.
    user_id = get_session(conn, :user_id)

    # We *always* set :current_user so that downstream plugs can rely on the
    # key existing.
    
    cond do
      # XXX: bypass for test cases - 1.8: Testing MVC / Integration Tests
      user = conn.assigns[:current_user] ->
        put_current_user(conn, user)

      # Get user details from the session.
      user = user_id && Rumbl.Accounts.get_user(user_id) ->
        put_current_user(conn, user)

      # Assign nil if all else fails.
      true ->
        assign(conn, :current_user, nil)
    end
  end

  def login(conn, user) do
    # Plug.Conn.put_session(conn, key, value)
    # Puts the specified value in the session for the given key.
    #
    # Plug.Conn.configure_session(conn, opts)
    # Configures the session.
    # Options:
    # - :renew  - generates a new session id for the cookie
    # - :drop   - drops the session, a session cookie will not be included in
    #             the response
    # - :ignore - ignores all changes made to the session in this request cycle
    conn
    |> put_current_user(user)
    |> put_session(:user_id, user.id)
    |> configure_session(renew: true)
  end

  def logout(conn) do
    # Drop the session.
    # Alternatively, we could keep the session, and just delete the
    # associated user id: `delete_session(conn, :user_id)`
    conn
    |> assign(:current_user, nil)
    |> configure_session(drop: true)
  end

  def authenticate_user(conn, _opts) do
    if conn.assigns.current_user do
      conn
    else
      # End the pipeline processing here if the user's not authenticated.
      conn
      |> put_flash(:error, "You must be logged in to access that page")
      |> redirect(to: Routes.page_path(conn, :index))
      |> halt()
    end
  end

  defp put_current_user(conn, user) do
    token = Phoenix.Token.sign(conn, "user socket", user.id)

    # Plug.Conn.assign(conn, key, value)
    # assign(t(), atom(), term()) :: t()
    #
    # Assigns a value to a key in the connection.
    #
    # The "assigns" storage is meant to be used to store values in the
    # connection so that other plugs in your plug pipeline can access them.
    # The assigns storage is a map.
    conn
    |> assign(:current_user, user)
    |> assign(:user_token, token)
  end
end
