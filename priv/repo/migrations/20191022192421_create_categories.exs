defmodule Rumbl.Repo.Migrations.CreateCategories do
  use Ecto.Migration

  def change do
    # $ mix phx.gen.schema Multimedia.Category categories name:string
    #
    # * creating lib/rumbl/multimedia/category.ex
    # * creating priv/repo/migrations/20191022192421_create_categories.exs
    #
    # Then edited to add non-null to `name`, and create the index.
    create table(:categories) do
      add :name, :string, null: false

      timestamps()
    end

    create unique_index(:categories, [:name])
  end
end
